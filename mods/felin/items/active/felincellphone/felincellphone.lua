require "/scripts/vec2.lua"

function init()
--	self.fireOffset = config.getParameter("fireOffset", {0,0})
--	self.flip = false
--	updateAim()
end

local released = false

function update(dt, fireMode, shiftHeld)
	if fireMode ~= "none" then
		if not released then
			fire()
			released = true
		end
		else
			released = false
	end
end

function uninit()	
end

function fire()
	objects = world.objectQuery(activeItem.ownerAimPosition(), 1)
	if objects[1] then
--	sb.logInfo(world.entityName(objects[1]))

		local translation = world.getObjectParameter(objects[1], "felinTranslation", nil)
		if translation then

			player.radioMessage({
				messageId = "felintranslation",
				unique = false,
				portraitImage = "/ai/portraits/felinportrait.png:talk.<frame>",
				text = translation
			})
--			-- Thanks Magicks
--			local interfaceConfig = root.assetJson("/interface/felincellphone.config")
--		interfaceConfig.gui.translationLabel.value = translation
--		activeItem.interact("ScriptPane", interfaceConfig)

--		activeItem.interact("ShowPopup", { title = "Felinvol-English", message = string.format("%s", translation), sound = "" })
		else
			world.sendEntityMessage(objects[1], "flip")
		end

		animator.setAnimationState("phone", "beep")
	end
end
