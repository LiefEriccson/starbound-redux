function init(args)
	object.setInteractive(true)
	object.setAllOutputNodes(animator.animationState("switchState") == "on")
end

function onInteraction(args)
	if animator.animationState("switchState") == "off" and object.isOutputNodeConnected(0) then
		animator.setAnimationState("switchState", "on")
		animator.playSound("on");
		object.setAllOutputNodes(true)
	else
		animator.setAnimationState("switchState", "off")
		animator.playSound("off");
		object.setAllOutputNodes(false)
	end
end
