function ageItem(baseItem, aging)
	isAddon = root.assetJson("/IFD_version.config:isAddon")

	baseItem.parameters.tooltipFields = baseItem.parameters.tooltipFields or {}
	baseTimeToRot = root.assetJson("/items/rotting.config:baseTimeToRot")

	if baseItem.parameters.timeToRot then

		if isAddon == true then
			if baseItem.parameters.timeToRot ~= baseTimeToRot then
				baseItem.parameters.timeToRot = baseTimeToRot
			end
			if baseItem.parameters.tooltipFields.rotTimeLabel ~= "" then
				baseItem.parameters.tooltipFields.rotTimeLabel = ""
			end
		else

			baseItem.parameters.timeToRot = baseItem.parameters.timeToRot - aging
			baseItem.parameters.tooltipFields.rotTimeLabel = getRotTimeDescription(baseItem.parameters.timeToRot)

			if baseItem.parameters.timeToRot <= 0 then
				local itemConfig = root.itemConfig(baseItem.name)
				return {
					name = itemConfig.config.rottedItem or root.assetJson("/items/rotting.config:rottedItem"),
					count = baseItem.count,
					parameters = {}
				}
			end
		end
	end

  return baseItem
end

function getRotTimeDescription(rotTime)
	local color = ""
	if rotTime <= 300 then color = "red;"
		elseif rotTime <= 1800 then color = "orange;"
		elseif rotTime <= 3600 then color = "yellow;"
		elseif rotTime <= 10800 then color = "green;"
		else color = "green;"
	end

	hours = string.format("%i", math.floor(rotTime/3600))
	minutes = string.format("%i", math.floor(rotTime/60 - (hours*60)))
	seconds = string.format("%i", math.floor(rotTime - hours*3600 - minutes*60))
	return "^" .. color .. "Spoils in: " .. hours .."h " .. "^reset;"
end
