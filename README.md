# Last Update: 3/26/2022

# Server Setup
## Ignore this section if you are NOT hosting a server
1. Extract `starbound-server-files.zip` to the directory you want to host the server on. 
2. Copy over the mods folder to the same directory you extracted the `starbound-server-files.zip` to.
3. Navigate to the `linux` folder. Then run `starbound_server`.


# Client Setup
If this is your first time playing Starbound, or only played Vanilla, here we go!

1. If you have loaded Starbound before, go to your local files (find Starbound in your Game Library on Steam -> right click -> Manage -> Browse Local Files) and **DELETE** `storage` folder. That folder contains data for your characters and worlds. If you wanna keep it, just rename the folder to something like I did in the screenshot below xD **If you are a first time player, I highly recommend reading everything under the `IMPORTANT` section.  Additionally, [go to this reddit link for more starting tips](https://www.reddit.com/r/starbound/comments/too5se/any_advice_for_a_new_player/)!
2. Now launch Starbound like you would any other game just to make sure it works. If you can get to the title screen successfully, it is time to move to the next step. 
3. Go to the [RELEASES page](https://gitlab.com/LiefEriccson/starbound-redux/-/releases) and **follow the directions there.**
4. Extract the folder into your Starbound folder. You can easily find this by going to Steam, find Starbound in your Game Library, then: right click -> Manage -> Browse Local Files. You want to extract the contents of that folder into here. It will look like this:

![SB Folder](https://i.imgur.com/Z6dJWge.png)

Ignore all the extra folders. I just have a lot of stuff from my many, many hours of playing Starbound lmao. You just want to make sure you see `client-thread-perf` and `mods` in the same view as `storage`. That is how you know you did it right!

5. Re-launch the game (Take a look at the IMPORTANT section below). When you get to the title screen, please check the box that allows for `asset mismatch` otherwise you might not be able to connect to the server.

![ ](https://i.imgur.com/AxBy1MA.png)

## Optimizations
Starbound is horribly optimized as is due to bad engine writing. There are not really many ways we can alleviate that unfortunately. However, there are a couple of potential solutions, but your mileage may vary based on your machine. 

### Easy Optimization change
These are simple changes that a few users report fixing some of the lag.

1. On the Starbound title screen, click on the `Options` and **Uncheck** the `Multiplayer Via Steam / Discord` option.
2. Disable Steam Overlay for Starbound. Go to your Steam Library, right click on Starbound -> Properties -> Uncheck `Enable the Steam Overlay while in-game`

### Harder Optimization Change
This is slightly more involved, but more users reported better performance gains.

**You must have an NVIDIA graphics card to perform this. If you do not, this will not be applicable for you.**

1. Go to the Starbound folder again (You can get here by click on `Browse local files` on Starbound's properties within Steam)
2. Go to the `storage` folder, right click on `starbound.config` and open with a text editor of your choice (notepad for example. Make sure you unselect `Always use this app to open .config files` if you see that option)
3. Within the `starbound.config` file, CTRL+F (it should be at the very bottom of the file). Change the value to `false`. The complete line should now look like: `"vsync" : false,` (Don't delete the comma). Save the file.
4. Right click anywhere on your desktop background on your computer and click on `NVIDIA Control Panel`.
5. Navigate to `Manage 3D Settings` on the left-hand list under `3D Settings`.
6. Click on the `Program Settings` tab and click on `Add`. When the new popup opens up, click on `Browse` at the bottom and go back to the ROOT starbound folder (You will see the `mods` and `storage` folder if you are in the right place.). Open the `win64` folder and select `starbound.exe` with the penguin icon.
7. On the main `NVIDIA Control Panel` window, the selected program should now show `Starbound (starbound.exe)`. Scroll down all the way to the bottom and then do the following:
    - Change `Triple Buffering` to `Off`
    - Change `Vertical Sync` to `On` **
    - Change `Virtual reality pre-rendered frames` to `1`
8. Make sure to click `Apply` at the bottom right once you finished.

![NVIDIA Settings](https://i.imgur.com/DA92PlP.png)


** Multiple options on this dropdown apparently make varying differences, but it will depend on your preferences since there are downsides associated with them. Play around with it and see what works best for you. You will notice immediately when you start playing. 

  - If you select `Adaptive (half refresh rate)`, you will supposedly have no more game stuttering. However, you will be locked to 30FPS only.
  - If you select `Adaptive`, there is a chance you will [see screen tearing like in this example video](https://youtu.be/rg_0D96ZGSY?t=18). It is very frustrating, but not all monitors will have this issue, but you will see less stutters than just selecting `On` as the option as outlined in Step 7 above.

# **IMPORTANT**

This game CAN (but sometimes it goes really fast...) take forever to load. Especially with mods. If you launch the game with steam and notice that nothing happens... this is normal.

It takes my own computer upwards of around 5-8 minutes of **NOTHING** happening before the game finally pops up and does the real load. Please be patient. 

Watch a youtube video or something c:

Orr... continue reading for a quick start guide!

Additionally, [read this reddit post for some additional getting started tips](https://www.reddit.com/r/starbound/comments/too5se/any_advice_for_a_new_player/)

# Suggested start!

### Quick notes

1. This game is POTENTIALLY hard. Like... I might have made it too difficult. LOL. I suggest us all to stick together and not get frustrated from the many, many deaths we will encounter. I believe we will start to outscale the monsters really hard later and this might not be applicable, but it's definitely punishing at the beginning.

2. As you might have guessed from point 3, the last time I played Starbound was a few years ago. A lot has changed, and I never used like half of these mods. Expect things to be broken, potentially run into unobtainable items, or worse: occasional crashing. 

3. Treat this server almost as a beta because I honestly don't know how balanced things will be o7

4. If you wanna play other versions of Starbound (like not modded for example), you just have to move the `mods` folder out of the `Starbound` folder and delete or rename the `storage` before starting the game C:

### Character creation

Most species during character creation has a mix of **positive and negative permanent perks** that will affect your gameplay. While they will not be gamechanging, some do have some rather large effects (some races have something like -40% resistance to certain elements...)

It is also important to note that, as far as my memory goes (which is not much at the moment),  there is **NO** way to change how your character looks after creating aside from putting on costumes and accessories. Your species, gender, and colors chosen will remain with you on that character forever. So take your time while you craft up your perfect character!

### Why is my ship broken??

After the very short intro mission, you will have the option to pick your ship type. I recommend choosing `Frackin Ship` as the base option as it offers more customization and follows the intended mod progression better than the vanilla experience will. 

![](https://i.imgur.com/tfKkhc1.png)

The actual visual layout you choose within the `Frackin Ship` option is your choice, however. As with character creation, I believe this is also a one way street (though you can technically dismantle everything later at some point), so choose carefully!

### Okay, now what? 

The absolute main mod in this custom modpack is `Frackin Universe`. It literally changes every single aspect and mechanic of the game to flesh it out more, give more customization, give more paths of progression, and greatly extends the early, mid, and late game. It is still constantly getting updated too, so I have no idea how to progress since there were major changes since the last time I played. While Minecraft Forge doesn't techincally change the core of Minecraft, Frackin Universe is essentially Starbound's version of Forge as it is so big that most other major mods ensure that they are compatible with it and base Starbound. Also removing the mod will require you to completely destroy your entire world and characters xD

In short, vanilla Starbound is vastly different than what you will play using my modpack.

The `quickbar` is also a godsend:

![Quick bar op! op!](https://i.imgur.com/UX7uK7v.png)


### Pick your class / role!

There is an RPG growth mod in here that actually adds an impressively large amount of gameplay changes. Unfortunately the UI for it is kind of bad and ~~I can't control that :x also some of the texts overlap and it's hard to see aaaa.~~ **EDIT: I FIXED MOST OF IT ACTUALLY!** But essentially, read up on each of the 6 available classes and pick one to specialize in!

![Wow! Classes!](https://i.imgur.com/k3fo67C.png)

This mod actually surprisingly changes how you play a lot and it's really cool since you learn skills and gain bonuses when using the correct weapon type. 

Unlike the other stuff in this guide, I believe you **CAN** reset your class and start over, but I completely forgot how or when that gets unlocked. 

### Make sure you research!

Once you unlock the `Personal Tricorder` (you get it very, very early in the game), you will gain access to `research`. This is essentially a giant skill tree from what I've seen so far, and you sacrifice certain materials in order to progress down the research path. It looks like basically every crafting recipe is locked behind research, so I recommend referring to this UI often and select one to focus on. Eventually, I'm sure we will all have mastered every category anyway. 

![so much research!](https://i.imgur.com/GtU9ZBt.png)
